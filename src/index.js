import express from 'express';
import cors from 'cors';

import sum from './task2A.js'
import processFio from './task2B.js'
import canonizeUsername from './task2C.js'
import canonizeColor from './task2D.js'
import xbox from './task2X.js'
import pcRouter from './task3A.js';


var app = express();
app.use(cors());

app.get('/', function (req, res) {
  res.json({
    greeting: 'use links for tasks decision',
    task2A: 'http://localhost:3000/task2A',
    task2B: 'http://localhost:3000/task2B',
    task2C: 'http://localhost:3000/task2C',
    task2D: 'http://localhost:3000/task2D',
    task2X: 'http://localhost:3000/task2X',
    task3A: 'http://localhost:3000/task3A',
  });
});

app.get('/task2A', function (req, res) {
  const a = req.query.a;
  const b = req.query.b;
  const result = sum(a,b);
  res.send(""+result);
});

app.get('/task2B', function (req, res) {
  const fullname = req.query.fullname;
  const result = processFio(fullname);
  res.send(""+result);
});

app.get('/task2C', function (req, res) {
  const username = req.query.username;
  const result = canonizeUsername(username);
  res.send(""+result);
});

app.get('/task2D', function (req, res) {
  const color = req.query.color;
  console.log('color '+  color);
  const result = canonizeColor(color);
  res.send(""+result);
});

app.get('/task2X', function (req, res) {
  const iparam = req.query.i;
  const result = xbox(iparam);
  res.send(""+result);
});

app.use('/task3A', pcRouter);


app.listen(3000, function () {
  console.log('Lesson2 app listening on port 3000!');
});
