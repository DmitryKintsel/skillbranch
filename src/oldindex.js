import express from 'express';
import fetch from 'isomorphic-fetch';

import canonize from './task2C.js'

const baseUrl = 'http://pokeapi.co/api/v2/pokemon/1';

var app = express();

app.get('/canonizeUsername', function (req, res) {
  const url = req.query.url;
  const username = canonize(url);

  res.json ({
    url,
    greeting: 'Hello, '+ username,
    username
  });
});

app.get('/', async (req, res) => {
  const response = await fetch(baseUrl);
  const result = await response.json();
  res.json({
    name: result.name,
    weight: result.weight
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
