import express from 'express';

var app = express();

function sum(a=0, b=0) {
  return +a + (+b);
}

app.get('/l2t1', function (req, res) {
  const a = req.query.a;
  const b = req.query.b;
  const result = sum(a,b);
  res.send(""+result);
});

app.listen(3000, function () {
  console.log('Lesson2 Task1 app listening on port 3000!');
});
