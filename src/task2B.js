//for task2B
const errorMessage = 'Invalid fullname';

export default function processFio(fio = '') {
  const regExp = new RegExp('\\s+');
  var terms = fio.split(regExp);

  if (terms.length == 1 && terms[0].length == 0 ) return errorMessage;

  if (terms[terms.length - 1].length == 0) {
    terms = terms.slice(0, terms.length - 1);
  }

  if (terms[0].length == 0) {
    terms = terms.slice(1, terms.length);
  }

  if (terms.length > 3) return errorMessage;

  //testing for invalid terms
  const regExp2 = new RegExp('[\\d_/!@#$%^&*(){}+=|?]');
  for (var i = 0; i<=terms.length-1; i++) {
    if (regExp2.test(terms[i])) return errorMessage;
  }

  //fix invalid capitalization
  var result = terms[terms.length-1].toLowerCase();
  result = result.charAt(0).toUpperCase() + result.slice(1);

  for (var i = 0; i<terms.length-1; i++) {
    result = result+ ' ' + terms[i].charAt(0).toUpperCase()+'.';
  }

  return result;
}


