//for task2C
const errorMessage = 'Invalid username';

export default function canonizeUsername(url='') {
  const regExp = new RegExp('@?(https:|http:)?(\/\/)?([a-z0-9.-]*(\/))?@?([a-z0-9._-]*)', 'i');
  const username = url.match(regExp);
  if (username.length<6 || username[5].length==0) return errorMessage;
  return '@'+username[5];
}
