//for task2D
const errorMessage = 'Invalid color';

export default function canonizeColor(inputValue = '') {
  //fix invalid capitalization
  var testColor = inputValue.toLowerCase().trim();
  testColor = unescape(testColor);

  var idxRgb = testColor.indexOf('rgb'); //test rgb model
  if (idxRgb > -1) {
    const regExp3 = new RegExp('rgb\\(\\s*\\d+\\s*,\\s*\\d+\\s*,\\s*\\d+\\s*\\)');
    var terms3 = testColor.match(regExp3);
    if (terms3 == null || terms3[0].length != testColor.length) return errorMessage;

    var testColor2 = testColor.substring(idxRgb + 3);
    const regExp2 = new RegExp('\\d+', 'g');
    const terms2 = testColor2.match(regExp2);
    if (terms2 == null || terms2.length != 3) return errorMessage;

    var r = (+terms2[0]);
    var g = (+terms2[1]);
    var b = (+terms2[2]);

    if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255) return errorMessage;

    var result = (r < 16 ? '#0' : '#') + r.toString(16)
      + (g < 16 ? '0' : '') + g.toString(16)
      + (b < 16 ? '0' : '') + b.toString(16);
    return result;
  }

  var idxHsl = testColor.indexOf('hsl'); //test hsl model
  if (idxHsl > -1) {
    const regExp3 = new RegExp('hsl\\(\\s*\\d+\\s*,\\s*\\d+\\%\\s*,\\s*\\d+\\%\\s*\\)');
    var terms3 = testColor.match(regExp3);
    if (terms3 == null || terms3[0].length != testColor.length) return errorMessage;

    var testColor2 = testColor.substring(idxHsl + 3);
    const regExp2 = new RegExp('\\d+', 'g');
    const terms2 = testColor2.match(regExp2);
    if (terms2 == null || terms2.length != 3) return errorMessage;

    var h = (+terms2[0]);
    var s = (+terms2[1]);
    var l = (+terms2[2]);

    console.log('h= ' + h);
    console.log('s = ' + s);
    console.log('l = ' + l);

    if (h < 0 || h > 359 || s < 0 || s > 100 || l < 0 || l > 100) return errorMessage;

    return hslToRgb(h / 360.0, s / 100.0, l / 100.0);
  }


  const regExp = new RegExp('#?[0-9a-f]{1,6}');
  const terms = testColor.match(regExp);

  if (terms == null || terms[0].length != testColor.length) return errorMessage;

  var result = terms[0].charAt(0) == '#' ? terms[0].substring(1) : terms[0];

  var len = result.length;
  if (len != 3 && len != 6)
    return errorMessage;

  if (len == 3)
    result = result.charAt(0) + result.charAt(0)
      + result.charAt(1) + result.charAt(1)
      + result.charAt(2) + result.charAt(2);
  result = '#' + result;

  console.log('result ' + result);
  return result;
}


function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    var hue2rgb = function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1.0 / 6) return p + (q - p) * 6 * t;
      if (t < 1.0 / 2) return q;
      if (t < 2.0 / 3) return p + (q - p) * (2.0 / 3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1.0 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1.0 / 3);
  }

  r = Math.round(r * 255);
  g = Math.round(g * 255);
  b = Math.round(b * 255);

  var result = (r < 16 ? '#0' : '#') + r.toString(16)
    + (g < 16 ? '0' : '') + g.toString(16)
    + (b < 16 ? '0' : '') + b.toString(16);
  return result;


}
