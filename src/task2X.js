//for task2X
const errorMessage = 'Invalid parameter';

export default function xbox(i = '') {

  var arr = [1, 18, 243, 3240, 43254,
    577368, 7706988, 102876480, 1373243544, 18330699168,
    244686773808, 3266193870720, 43598688377184, 581975750199168, 7768485393179328,
    103697388221736960, 1384201395738071424, 18476969736848122368, 246639261965462754048];
  var result= ((+i)>=0 && (+i)<arr.length) ? arr[i] : 0;

  // switch (+i) {
  //   case 0 : result = 1; break;
  //   case 1 : result = 18; break;
  //   case 2 : result = 243; break;
  //   case 3 : result = 3240; break;
  //   case 4 : result = 43254; break;
  //   case 5 : result = 577368; break;
  //   case 6 : result = 7706988; break;
  // }

  console.log('result ' + result);
  return result;
}
