import fetch from 'isomorphic-fetch';
import express from 'express';

//for task3A
const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
const notFoundMessage = 'Not Found';
const volumesFunctionName = '/volumes';

const pcRouter = express.Router();

pcRouter.get('/*', function (req, res) {
  const url = req.url;

  if (url == volumesFunctionName) {
    res.send(getVolumes(url));
    return;
  }
  const result = getPcData(pc, url.substring(1, url.length));
  if (result == notFoundMessage) {
    res.status(404).send(notFoundMessage)
  }
  else
    res.json(result);
});

module.exports = pcRouter;

let pc = {};

fetch(pcUrl)
  .then(async(res) => {
    pc = await res.json();
    console.log('gets a pc structure');
  })
  .catch(err => {
    console.error('Can\'t get a pc structure data:', err);
  });

function getVolumes(url) {
  var volumes = {};
  pc.hdd.forEach(function (item, i) {
    var key = item.volume;
    if (volumes[key]) {
      volumes[key] += (+item.size);
    }
    else
      volumes[key] = (+item.size);
  });

  var result = {};
  for (var key in volumes) {
    result[key] = volumes[key] + "B";
  }
  return result;
}

export default function getPcData(data, url = '') {
  if (url) {
    var pos = url.indexOf('/');
    if (pos > 0) {
      var current = url.substring(0, pos);
      var nextPart = url.substring(pos + 1);
      if (typeof data[current] != "undefined") {
        return getPcData(data[current], nextPart);
      }
      else return notFoundMessage;
    }
    else {
      if (data.hasOwnProperty(url) && data.constructor()[url] == undefined) {
        return getPcData(data[url]);
      }
      else
        return notFoundMessage;
    }
  }
  return data;
}
