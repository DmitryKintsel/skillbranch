import canonizeUsername from './task2C.js';

const linksArray = [
  '//telegram.me/skillbranch0',
  'telegram.me/skillbranch1',
  'http://vk.com/skillbranch2',
  'https://vk.com/skillbranch3',
  'https://vk.com/skillbranch4',
  'Https://vk.com/skillbranch5',
  'https://VK.com/skillbranch6',
  'https://vk.COM/skillbranch7',
  'https://vk.com/SkillBranch8',
  'https://vk.com/skillbranch9?wall-6997',
  '//telegram.me/skillbranch10',
  'vk.com/skillbranch11',
  'skillbranch12',
  '@skillbranch13',
  'www.vk.com/skillbranch14',
  'http://xn--80adtgbbrh1bc.xn--p1ai/skill.branch15',
  'www.vk.com/@skillbranch16'
];


linksArray.forEach(function(item, i) {
  console.log(canonizeUsername(item));
});

